#Never Tick Alone

Suspends dedicated server world's ticks when no players are online.

Server-side-only for Minecraft 1.17.1 with the fabric loader.

### Development Setup

For setup instructions please see the [fabric wiki page](https://fabricmc.net/wiki/tutorial:setup) that relates to the IDE that you are using.

