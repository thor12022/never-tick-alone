package thor12022.never_tick_alone;

import net.fabricmc.api.DedicatedServerModInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NeverTickAlone implements DedicatedServerModInitializer
{
   public static final Logger LOGGER = LogManager.getLogger("never_tick_alone");
   
   @Override
   public void onInitializeServer()
   {
      LOGGER.info("Had nothing to do, and did it well.");
   }
}
