package thor12022.never_tick_alone.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.server.MinecraftServer;

@Mixin(MinecraftServer.class)
public interface MinecraftServerAccessor
{
   @Accessor("timeReference")
   public void setTimeReference(long val);

   @Accessor("nextTickTimestamp")
   public void setNextTickTimestamp(long val);
}