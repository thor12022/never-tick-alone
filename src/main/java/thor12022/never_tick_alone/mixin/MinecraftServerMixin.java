package thor12022.never_tick_alone.mixin;

import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Util;

@Mixin(MinecraftServer.class)
class MinecraftServerMixin
{
   private static final long LONG_TICK_MS = 333L;
   
   @SuppressWarnings("static-method")
   @Redirect(method = "runServer()V", at = @At(value = "FIELD", target = "Lnet/minecraft/server/MinecraftServer;timeReference:J", opcode = Opcodes.PUTFIELD))
   private void injectTimeReference(MinecraftServer server, long val)
   {
      if(server.getCurrentPlayerCount() == 0)
      {
         ((MinecraftServerAccessor)server).setTimeReference(server.getTimeReference() + LONG_TICK_MS);
      }
      else
      {
         ((MinecraftServerAccessor)server).setTimeReference(val);
      }
   }
   

   @SuppressWarnings("static-method")
   @Redirect(method = "runServer()V", at = @At(value = "FIELD", target = "Lnet/minecraft/server/MinecraftServer;nextTickTimestamp:J", opcode = Opcodes.PUTFIELD))
   private void injectNextTime(MinecraftServer server, long val)
   {
      if(server.getCurrentPlayerCount() == 0)
      {
         ((MinecraftServerAccessor)server).setNextTickTimestamp( Math.max(Util.getMeasuringTimeMs() + LONG_TICK_MS, server.getTimeReference()) );
      }
      else
      {
         ((MinecraftServerAccessor)server).setNextTickTimestamp(val);
      }
   }
}
