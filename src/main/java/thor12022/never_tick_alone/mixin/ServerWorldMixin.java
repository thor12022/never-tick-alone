package thor12022.never_tick_alone.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.server.world.ServerWorld;

@Mixin(ServerWorld.class)
class ServerWorldMixin
{
   @Inject(method = "tick", at = @At("HEAD"), cancellable=true)
   private void onTick(final CallbackInfo info)
   {
      if(((ServerWorld)(Object)this).getServer().getCurrentPlayerCount() == 0)
      {
         info.cancel();
         return;
      }
   }

}
